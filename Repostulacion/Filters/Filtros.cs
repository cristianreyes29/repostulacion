﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Repostulacion.Models;
using System.Collections.Generic;
using System.Linq;

namespace Repostulacion.Filters
{
    /// <summary>
    /// Clase que permite comprobar si un determinado 
    /// metodo o funcionalidad tiene acceso o no a un modulo del sistema
    /// 
    /// </summary>
    public class Permiso : ActionFilterAttribute
    {
        public int ControladorID { get; set; }
        public int MetodoID { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            
            bool result = false;
            
            if (ControladorID > 0 && MetodoID > 0)
            {
                List<relRolControladorMetodo> lst = filterContext.HttpContext.Session.GetObject<List<relRolControladorMetodo>>("rrcm");

                if (lst != null)
                {
                    foreach (relRolControladorMetodo rel in lst)
                    {
                        if (rel.vMetodo.Codigo == MetodoID && rel.vControlador.Codigo == ControladorID)
                        {
                            result = true;
                        }
                    }
                }
            }
            
            if (!result)
            {

                RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                redirectTargetDictionary.Add("action", "Index");
                redirectTargetDictionary.Add("controller", "Home");

                filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
            }
            
            base.OnActionExecuting(filterContext);
        }
    }

    public static class UtilidadesFiltros
    {
        /// <summary>
        /// Metodo utilizado para comprobar si un metodo
        /// o funcionalidad de sistema puede ser visto 
        /// por el usuario actual
        /// </summary>
        /// <param name="lst"> Lista de permisos</param>
        /// <param name="controlador"> ID del controlador</param>
        /// <param name="metodo"> ID del metodo</param>
        /// <returns> Retorna un bool indicando si puede o no ver la funcionalidadf</returns>
        public static bool puedePermiso(List<relRolControladorMetodo> lst, int controlador, int metodo)
        {

            return (from rel in lst
                    where rel.ControladorID == controlador &&
                    rel.MetodoID == metodo
                    select rel).FirstOrDefault() != null ? true : false;
        }
    }
}
