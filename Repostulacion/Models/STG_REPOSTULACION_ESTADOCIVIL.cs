﻿using System.ComponentModel.DataAnnotations;

namespace Repostulacion.Models
{
    public class STG_REPOSTULACION_ESTADOCIVIL
    {
        public int ID { get; set; }

        [Required]
        public int CODIGO_ESTADO { get; set; }

        public string DETALLE { get; set; }
    }
}
