﻿using System.ComponentModel.DataAnnotations;

namespace Repostulacion.Models
{
    public class STG_REPOSTULACION_CARRERAS
    {
        public int ID { get; set; }

        [Required]
        public int ID_CARRERA { get; set; }

        public string CODIGO_CARRERA { get; set; }

        public string NOMBRE_CARRERA { get; set; }

        public int? NEM { get; set; }

        public int? RANKING { get; set; }

        public int? LYC { get; set; }

        public int? MATEMATICAS { get; set; }

        public int? HISTORIA_Y_GEOGRAFIA { get; set; }

        public int? CIENCIAS { get; set; }

        public int? CUPOS { get; set; }

        public int? EXCEDENTES { get; set; }

        public int? C_T_INGESTAR { get; set; }

        public int? C_T_INGESPDD { get; set; }

        public int? C_T_PROPEDEU { get; set; }

        public int? C_T_NORMAL { get; set; }

        public int? C_T_INGESHFU { get; set; }

        public int? C_T_INGBACHI { get; set; }

        public int? C_T_OFICIO { get; set; }

        public int? C_T_INGBEA { get; set; }

        public int? C_T_RENDIC { get; set; }

        public int? C_T_INGCOU { get; set; }

        public int? C_T_INGPACE { get; set; }

        public int? C_T_INGESPED { get; set; }

        public int? C_T_MIETNICA { get; set; }

        public int? C_T_ING_ES15 { get; set; }

        public int? C_T_INGESTFU { get; set; }

        public int? C_R_INGESTAR { get; set; }

        public int? C_R_INGESPDD { get; set; }

        public int? C_R_PROPEDEU { get; set; }

        public int? C_R_NORMAL { get; set; }

        public int? C_R_INGESHFU { get; set; }

        public int? C_R_INGBACHI { get; set; }

        public int? C_R_OFICIO { get; set; }

        public int? C_R_INGBEA { get; set; }

        public int? C_R_RENDIC { get; set; }

        public int? C_R_INGCOU { get; set; }

        public int? C_R_INGPACE { get; set; }

        public int? C_R_INGESPED { get; set; }

        public int? C_R_MIETNICA { get; set; }

        public int? C_R_ING_ES15 { get; set; }

        public int? C_R_INGESTFU { get; set; }
    }
}
