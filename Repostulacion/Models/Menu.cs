﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repostulacion.Models
{
    [Table("Menu")]
    public class Menu
    {
        public int ID { get; set; }

        [Required]
        public string Nombre { get; set; }
    }

    [Table("relRolMenu")]
    public class relRolMenu
    {
        [Required]
        public int RolID { get; set; }
        public virtual Rol vRol { get; set; }

        [Required]
        public int MenuID { get; set; }
        public virtual Menu vMenu { get; set; }
    }
}
