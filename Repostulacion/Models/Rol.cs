﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repostulacion.Models
{
    [Table("Rol")]
    public class Rol
    {
        public int ID { get; set; }

        [Required]
        public string Nombre { get; set; }
    }

    [Table("relUsuarioRol")]
    public class relUsuarioRol
    {
        [Required]
        public int UsuarioID { get; set; }
        public virtual Usuario vUsuario { get; set; }

        [Required]
        public int RolID { get; set; }
        public virtual Rol vRol { get; set; }
    }
}
