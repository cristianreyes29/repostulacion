﻿using System.ComponentModel.DataAnnotations;

namespace Repostulacion.Models
{
    public class STG_REPOSTULACION_COMUNAS
    {
        public int ID { get; set; }

        [Required]
        public int ID_COMUNA { get; set; }

        public int? CODIGO { get; set; }

        public string NOMBRE { get; set; }

        public int? ID_PROVINCIA { get; set; }
    }
}
