﻿namespace Repostulacion.Models
{
    public class STG_DEMRE_ARCHIVO_C
    {
        public int ID { get; set; }

        public string TIPO_IDENTIFICACION { get; set; }

        public string RUT { get; set; }

        public string DV { get; set; }

        public int? ANNIO_PROCESO { get; set; }

        public int? SITUACION_EGRESO_EDUCACIONAL { get; set; }

        public int? LOCAL_EDUCACIONAL { get; set; }

        public int? UNIDAD_EDUCATIVA { get; set; }

        public int? RBD { get; set; }

        public string RAMA_EDUCACIONAL { get; set; }

        public int? GRUPO_DEPENDENCIA { get; set; }

        public int? CODIGO_REGION { get; set; }

        public int? CODIGO_PROVINCIA { get; set; }

        public int? ANNIO_EGRESO_E_MEDIA { get; set; }

        public string BEA { get; set; }

        public float? PROMEDIO_NOTAS { get; set; }

        public int? PUNTAJE_NEM { get; set; }

        public int? PUNTAJE_RANKING { get; set; }

        public int? PTJE_LENGUAJE_COMUNIC_ACTUAL { get; set; }

        public int? PTJE_MATEMATICA_ACTUAL { get; set; }

        public int? PTJE_HIST_CS_SOCIALES_ACTUAL { get; set; }

        public int? PTJE_CIENCIAS_ACTUAL { get; set; }

        public string MODULO_CIENCIAS_ACTUAL { get; set; }

        public float? PROMEDIO_LENG_MAT_ACTUAL { get; set; }

        public int? PTJE_LENGUAJE_COMUNIC_ANTERIOR { get; set; }

        public int? PTJE_MATEMATICAS_ANTERIOR { get; set; }

        public int? PTJE_HIST_CS_SOCIALES_ANTERIOR { get; set; }

        public int? PTJE_CIENCIAS_ANTERIOR { get; set; }

        public string MODULO_CIENCIAS_ANTERIOR { get; set; }

        public float? PROMEDIO_LENG_MAT_ANTERIOR { get; set; }

        public int? PAA_VERBAL { get; set; }

        public int? PAA_MATEMATICA { get; set; }

        public int? PCE_HISTORIA { get; set; }

        public int? PCE_BIOLOGIA { get; set; }

        public int? PCE_CS { get; set; }

        public int? PCE_FISICA { get; set; }

        public int? PCE_MATEMATICA { get; set; }

        public int? PCE_QUIMICA { get; set; }

        public float? PROMEDIO_PAA { get; set; }

        public int? PERCENTIL_LM_ANTERIOR { get; set; }

        public int? PERCENTIL_LM_ACTUAL { get; set; }

        public string HABILITA_PEDAGOGIA { get; set; }

        public string PACE { get; set; }

        public int? COD_COMUNA { get; set; }

        public int? COD_ENSENANZA { get; set; }

        public string PERCENTIL_NOTAS { get; set; }

        public int? REPOSTULANTE { get; set; }
    }
}
