﻿namespace Repostulacion.Models
{
    public class STG_DEMRE_ARCHIVO_B
    {
        public int ID { get; set; }

        public string TIPO_IDENTIFICACION { get; set; }

        public string RUT { get; set; }

        public string DIGITO_VERIFICADOR { get; set; }

        public int? AGNO_PROCESO { get; set; }

        public string APELLIDO_PATERNO { get; set; }

        public string APELLIDO_MATERNO { get; set; }

        public string NOMBRES { get; set; }

        public string NACIONALIDAD { get; set; }

        public int? GENERO { get; set; }

        public int? LOCAL_EDUCACIONAL { get; set; }

        public int? UNIDAD_EDUCATIVA { get; set; }

        public int? RBD { get; set; }

        public int? CODIGO_REGION { get; set; }

        public int? CODIGO_PROVINCIA { get; set; }

        public int? ANNIO_EGRESO_E_MEDIA { get; set; }

        public string FECHA_NACIMIENTO { get; set; }

        public int? ESTADO_CIVIL { get; set; }

        public int? TIENE_TRABAJO_REMUNERADO { get; set; }

        public int? HORARIO_TRABAJO { get; set; }

        public int? HORAS_DEDICA_TRABAJO { get; set; }

        public int? VIVE_CON { get; set; }

        public int? GRUPO_FAMILIAR { get; set; }

        public int? TRABAJAN_GRUPO_FAMILIAR { get; set; }

        public int? JEFE_FAMILIA { get; set; }

        public int? FINANCIA_PRIMARIO_ESTUDIOS { get; set; }

        public int? ESTUD_GRUPO_FAMILIAR_PREBASICA { get; set; }

        public int? INGRESO_BRUTO_FAMILIAR { get; set; }

        public int? COBERTURA_SALUD { get; set; }

        public int? VIVEN_PADRES { get; set; }

        public int? EDUCACION_PADRE { get; set; }

        public int? EDUCACION_MADRE { get; set; }

        public int? SIT_OCUPACIONAL_PADRE { get; set; }

        public int? SIT_OCUPACIONAL_MADRE { get; set; }

        public int? TIPO_ORGANISMO_TRABAJO_PADRE { get; set; }

        public int? TIPO_ORGANISMO_TRABAJO_MADRE { get; set; }

        public int? OCUPACION_PRINCIPAL_PADRE { get; set; }

        public int? OCUPACION_PRINCIPAL_MADRE { get; set; }

        public int? RAMA_OCUPACIONAL_PADRE { get; set; }

        public int? RAMA_OCUPACIONAL_MADRE { get; set; }

        public string RUT_PADRE { get; set; }

        public string RUT_MADRE { get; set; }

        public string DIR_CALLE { get; set; }

        public string DIR_NUMERO { get; set; }

        public string DIR_BLOCK { get; set; }

        public string DIR_DEPATAMENTO { get; set; }

        public string DIR_VILLA_POBLACION { get; set; }

        public int? DIR_CODIGO_REGION { get; set; }

        public int? DIR_CODIGO_PROVINCIA { get; set; }

        public int? DIR_CODIGO_COMUNA { get; set; }

        public string DIR_NOMBRE_PROVINCIA { get; set; }

        public string DIR_NOMBRE_COMUNA { get; set; }

        public string DIR_CIUDAD { get; set; }

        public string DIR_CODIGO_AREA { get; set; }

        public string DIR_NUMERO_TELEFONO { get; set; }

        public string DIR_PREFIJO_CELULAR { get; set; }

        public string DIR_NUMERO_CELULAR { get; set; }

        public string DIR_EMAIL { get; set; }

        public int? NRO_TARJETA_MATRICULA { get; set; }

        public string BEA { get; set; }

        public string CODIGO_INST_SUPERIOR_ANTERIOR { get; set; }

        public int? INSCRITO_PROCESO { get; set; }

        public int? FINANCIA_SECUNDARIO_ESTUDIOS { get; set; }

        public int? ESTUD_GRUPO_FAMILIAR_BASICA { get; set; }

        public int? ESTUD_GRUPO_FAMILIAR_MEDIA1_3 { get; set; }

        public int? ESTUD_GRUPO_FAMILIAR_MEDIA4 { get; set; }

        public int? ESTUD_GRUPO_FAMILIAR_SUPERIOR { get; set; }

        public int? ESTUD_GRUPO_FAMILIAR_OTROS { get; set; }

        public string SECRETARIA_ADMISION { get; set; }

        public string PACE { get; set; }

        public int? CODIGO_COMUNA { get; set; }

        public int? COD_ENSENANZA { get; set; }

        public string NOMBRE_PROVINCIA { get; set; }

        public string NOMBRE_COMUNA { get; set; }

        public int? RINDIO_PROCESO { get; set; }

        public int? ANNIO_LLEGA_CHILE { get; set; }

        public int? TIENE_HIJOS { get; set; }

        public int? CUANTOS_HIJOS { get; set; }

        public int? VIVE_CON_HIJOS { get; set; }

        public int? EMBARAZO_ADOLESCENTE { get; set; }

        public string CODIGO_ETNIA { get; set; }

        public string OCUPACION { get; set; }

        public string DISCAPACIDAD { get; set; }

        public int? TIPO_DISCAPACIDAD { get; set; }

        public int? ECONOMICAMENTE { get; set; }

        public string OTRO_JEFE_FAMILIA { get; set; }

        public string COMPLETO_EDUCACION_MADRE { get; set; }

        public string COMPLETO_EDUCACION_PADRE { get; set; }

        public string CARRERA_INSTITUCION_SUPERIOR { get; set; }

        public string TITULACION_CARRERA { get; set; }

        public int? ACTIVIDAD_JEFE_FAMILIA { get; set; }

        public int? PERSONAS_ESTUDIAN_SUP { get; set; }

        public int? COD_NACIONALIDAD { get; set; }

        public int? REPOSTULANTE { get; set; }

        public string ESTUDIO_INST_SUPERIOR { get; set; }

        public int? TIPO_INSTITUCION { get; set; }

        public string DISCAPACIDAD_VISUAL { get; set; }

        public string DISCAPACIDAD_AUDITIVA { get; set; }

        public string DISCAPACIDAD_FISICA { get; set; }

        public string DISCAPACIDAD_PSIQUICA { get; set; }

        public string DISCAPACIDAD_INTELECTUAL { get; set; }

        public string SOLICITA_ADECUACION { get; set; }

        public string RECIBIO_ADECUACION_COLEGIO { get; set; }

        public string ACEPTA_CONTACTO_DEMRE { get; set; }

        public string IDIOMA_ESPANOL { get; set; }

        public string IDIOMA_INGLES { get; set; }

        public string IDIOMA_FRANCES { get; set; }

        public string IDIOMA_ALEMAN { get; set; }

        public string IDIOMA_ITALIANO { get; set; }

        public string IDIOMA_PORTUGUES { get; set; }

        public string IDIOMA_CHINO { get; set; }

        public string IDIOMA_OTRO { get; set; }

        public string DESC_OTRO_IDIOMA { get; set; }

        public int? NIVEL_ESPANOL { get; set; }

        public int? NIVEL_INGLES { get; set; }

        public int? NIVEL_FRANCES { get; set; }

        public int? NIVEL_ALEMAN { get; set; }

        public int? NIVEL_ITALIANO { get; set; }

        public int? NIVEL_PORTUGUES { get; set; }

        public int? NIVEL_CHINO { get; set; }

        public int? NIVEL_OTRO { get; set; }

        public string ACTIVIDAD_VOLUNTARIADO { get; set; }

        public string ACT_TRABAJO_NO_REMUNERADO { get; set; }

        public string ACT_AGRUPACION_DEPORTIVA { get; set; }

        public string ACT_AGRUPACION_RELIGIOSA { get; set; }

        public string ACT_AGRUPACION_POLITICA { get; set; }

        public string ACTIVIDA_NINGUNA { get; set; }

        public string PREPARACION_SITIO_DEMRE { get; set; }

        public string PREPARACION_PREU_PAGADO { get; set; }

        public string PREPARACION_PREU_GRATUITO { get; set; }

        public string PREPARACION_COLEGIO { get; set; }

        public string PREPARACION_ESTUDIO_PERSONAL { get; set; }

        public string PREPARACION_PROF_PARTICULAR { get; set; }

        public string PREPARACION_NINGUNA { get; set; }

        public string POSTULAR_GRATUIDAD { get; set; }

        public string POSTULAR_BECA { get; set; }

        public string POSTULAR_CREDITO_SOLIDARIO { get; set; }

        public string POSTULAR_CAE { get; set; }

        public string POSTULAR_OTRO_TIPO_CRED { get; set; }

        public string POSTULAR_NINGUN_FINAN { get; set; }

        public string DESC_OTRO_TIPO_CRED { get; set; }

        public int? COD_COMUNA { get; set; }

        public string PUBLICA_PTJE_NAC { get; set; }

        public string PUBLICA_PTJE_COL { get; set; }

        public string NOMBRE_SOCIAL { get; set; }

        public string PUBLICA_NOMBRE_SOCIAL { get; set; }

        public string COINCIDE_GENERO { get; set; }

        public string ZIP_CODE { get; set; }

        public int? DOMINA_LENGUA_INDIGENA { get; set; }

        public string TEXTO_ADECUACION { get; set; }

        public int? ZONA { get; set; }


    }
}
