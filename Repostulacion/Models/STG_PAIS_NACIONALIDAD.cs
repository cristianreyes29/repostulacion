﻿namespace Repostulacion.Models
{
    public class STG_PAIS_NACIONALIDAD
    {
        public int ID { get; set; }

        public int? AGNO_PROCESO { get; set; }

        public string DOMINIO { get; set; }

        public int? CODIGO { get; set; }

        public string DESCRIPCION { get; set; }
    }
}
