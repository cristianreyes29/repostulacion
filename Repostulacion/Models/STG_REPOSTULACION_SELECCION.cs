﻿namespace Repostulacion.Models
{
    public class STG_REPOSTULACION_SELECCION
    {
        public int ID { get; set; }

        public string CARRERA { get; set; }

        public int? ANIO_INGRESO { get; set; }

        public int? TIPO_INGRESO { get; set; }

        public string  RUT { get; set; }

        public float PONDERADO { get; set; }

        public int? OPORTUNIDAD { get; set; }

        public int? ELIMINADO { get; set; }
    }
}
