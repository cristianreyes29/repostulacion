﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repostulacion.Models
{

    [Table("Controlador")]
    public class Controlador
    {
        [Display(Name = "Código")]
        public int ID { get; set; }

        [Required]
        public int Codigo { get; set; }

        [Required]
        public string Nombre { get; set; }
    }

    [Table("Metodo")]
    public class Metodo
    {
        [Display(Name = "Código")]
        public int ID { get; set; }

        [Required]
        public int Codigo { get; set; }

        [Required]
        public string Nombre { get; set; }
    }

    [Table("relControladorMetodo")]

    public class relControladorMetodo
    {
        [Required]
        public int ControladorID { get; set; }
        public virtual Controlador vControlador { get; set; }

        [Required]
        public int MetodoID { get; set; }
        public virtual Metodo vMetodo { get; set; }
    }

    [Table("relRolControladorMetodo")]
    public class relRolControladorMetodo
    {
        [Required]
        public int RolID { get; set; }
        public virtual Rol vRol { get; set; }

        [Required]
        public int ControladorID { get; set; }
        public virtual Controlador vControlador { get; set; }

        [Required]
        public int MetodoID { get; set; }
        public virtual Metodo vMetodo { get; set; }
    }

}
