﻿using System.ComponentModel.DataAnnotations;

namespace Repostulacion.Models
{
    public class STG_REPOSTULACION_CARR_POND
    {
        public int ID { get; set; }

        [Required]
        public string RUT { get; set; }

        [Required]
        public int ANIO_INGRESO { get; set; }

        public int? TIPO_INGRESO { get; set; }

        public string CARRERA1 { get; set; }

        public string CARRERA2 { get; set; }

        public string FECHA_INGRESO { get; set; }

        public string INSCRITO_OTRA_UNIVERSIDAD { get; set; }

        public float PONDERADO1 { get; set; }

        public float PONDERADO2 { get; set; }

        public string RAMA { get; set; }

        public int? ANTECEDENTES { get; set; }

        public int? EXAMEN { get; set; }

        public int? REPOSTULANTE { get; set; }

        public string FECHA_MODIFICACION { get; set; }

        public int? ELIMINAR { get; set; }

        public float POND_ADICIONAL1 { get; set; }

        public float POND_ADICIONAL2 { get; set; }
    }
}
