﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repostulacion.Models
{
    [Table("Usuario")]
    public class Usuario
    {
        public int ID { get; set; }

        [Required]
        public string NombreUsuario { get; set; }
        
    }
}
