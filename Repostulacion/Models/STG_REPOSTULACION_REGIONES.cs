﻿using System.ComponentModel.DataAnnotations;

namespace Repostulacion.Models
{
    public class STG_REPOSTULACION_REGIONES
    {
        public int ID { get; set; }

        [Required]
        public int ID_REGION { get; set; }

        public string CODIGO { get; set; }

        public string NOMBRE { get; set; }
    }
}
