﻿using System.ComponentModel.DataAnnotations;

namespace Repostulacion.Models
{
    public class STG_REPOSTULACION_REPOSTULANTE
    {
        public int ID { get; set; }

        [Required]
        public int ID_TIPO { get; set; }

        public string DETALLE { get; set; }
    }
}
