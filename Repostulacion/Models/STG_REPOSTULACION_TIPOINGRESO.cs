﻿using System.ComponentModel.DataAnnotations;

namespace Repostulacion.Models
{
    public class STG_REPOSTULACION_TIPOINGRESO
    {
        public int ID { get; set; }

        [Required]
        public int ID_TIPO { get; set; }

        public string CODIGO { get; set; }

        public string NOMBRE { get; set; }
        
        public int? HABILITADO { get; set; }

        public int? ESPECIAL_REPOSTULACION { get; set; }
    }
}
