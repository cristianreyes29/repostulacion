﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repostulacion.Data;
using Repostulacion.Models;

namespace Repostulacion.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(
            ApplicationDbContext context)
        {

            _context = context;
        }
        
        public IActionResult Index()
        {
            
            List<relRolControladorMetodo> lst = HttpContext.Session.GetObject<List<relRolControladorMetodo>>("rrcm");

            if (lst != null)
            {
                // Se establecen variables viewbag de tipo bool para indicar si se muestran o no
                // los modulos correspondientes

                

            }
            return View();
        }

        [Authorize]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
