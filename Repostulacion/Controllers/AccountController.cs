﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authentication.Cookies;
using Repostulacion.Data;
using Repostulacion.Models;
using System.Linq;
using System.Collections.Generic;

namespace Repostulacion.Controllers
{
    /// <summary>
    /// Controlador para manejar el inicio y cierre de sesión de las cuentas de usuario
    /// </summary>
    public class AccountController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AccountController(
            ApplicationDbContext context)
        {
            _context = context;
        }
       
        /// <summary>
        /// Metodo que realiza el logeo con azure AD 
        /// devuelve al método ComprobarUsuario de Account
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult SignIn()
        {
            var redirectUrl = Url.Action(nameof(AccountController.ComprobarUsuario), "Account");
            IActionResult result = Challenge(
                new AuthenticationProperties { RedirectUri = redirectUrl },
                OpenIdConnectDefaults.AuthenticationScheme);

            return result;
        }
        

        /// <summary>
        /// Metodo encargado de comprobar si el usuario logeado con azure AD
        /// existe en la tabla de usuario del sistema y ademas se obtiene la
        /// lista permisos de metodos por controlador para los roles del usuario
        /// </summary>
        /// <returns></returns>
        public IActionResult ComprobarUsuario()
        {

            // Comprobación si el usuario de azure AD está autenticado
            if (User.Identity.IsAuthenticated)
            {
                
                Usuario usuario = (from u in _context.Usuarios
                                   where u.NombreUsuario.Equals(User.Identity.Name)
                                   select u).FirstOrDefault();

                if (usuario != null)
                {
                    List<relUsuarioRol> lstRelUsuarioRol = (from r in _context.relUsuarioRol
                                                            where r.UsuarioID == usuario.ID
                                                            select r).ToList();

                    List<relRolControladorMetodo> lstRelRolControladorMetodo = new List<relRolControladorMetodo>();
                    foreach (relUsuarioRol rel in lstRelUsuarioRol)
                    {
                        List<relRolControladorMetodo> lst = (from r in _context.relRolControladorMetodo
                                                             where r.RolID == rel.RolID
                                                             select r).ToList();

                        foreach (relRolControladorMetodo rel2 in lst)
                        {
                            lstRelRolControladorMetodo.Add(rel2);
                        }
                    }

                    // El usuario está autenticado con el sistema
                    HttpContext.Session.SetObject("IsAuthenticated", true);

                    // Lista que contiene todos los permisos para el usuario autenticado
                    HttpContext.Session.SetObject("rrcm", lstRelRolControladorMetodo);

                    return RedirectToAction("Index", "Home");
                }
                
            }

            return RedirectToAction("SignedOut2", "Account");
        }
        
        /// <summary>
        /// Metodo que permite deslogear al usuario de azure AD
        /// lo devuelve a la vista de SignedOut para indicar su salida
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult SignOut()
        {
            var callbackUrl = Url.Action(nameof(SignedOut), "Account", values: null, protocol: Request.Scheme);
            return SignOut(
                new AuthenticationProperties { RedirectUri = callbackUrl },
                CookieAuthenticationDefaults.AuthenticationScheme,
                OpenIdConnectDefaults.AuthenticationScheme);
        }
        
        

        [HttpGet]
        public IActionResult SignedOut2()
        {
            return View();
        }

        [HttpGet]
        public IActionResult SignedOut()
        {

            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }

           
            HttpContext.Session.SetObject("rrcm", null);
            
            HttpContext.Session.SetObject("IsAuthenticated", false);

            return View();
        }

        
    }
}
