﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Repostulacion.Migrations
{
    public partial class tablasstage4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "HABILITADO",
                table: "STG_REPOSTULACION_TIPOINGRESO",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ESPECIAL_REPOSTULACION",
                table: "STG_REPOSTULACION_TIPOINGRESO",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TIPO_INGRESO",
                table: "STG_REPOSTULACION_SELECCION",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "OPORTUNIDAD",
                table: "STG_REPOSTULACION_SELECCION",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ELIMINADO",
                table: "STG_REPOSTULACION_SELECCION",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ANIO_INGRESO",
                table: "STG_REPOSTULACION_SELECCION",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ID_REGION",
                table: "STG_REPOSTULACION_PROVINCIAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO",
                table: "STG_REPOSTULACION_PROVINCIAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ID_PROVINCIA",
                table: "STG_REPOSTULACION_COMUNAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO",
                table: "STG_REPOSTULACION_COMUNAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "RANKING",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "NEM",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "MATEMATICAS",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "LYC",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "HISTORIA_Y_GEOGRAFIA",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "EXCEDENTES",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_RENDIC",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_PROPEDEU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_OFICIO",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_NORMAL",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_MIETNICA",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_ING_ES15",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGPACE",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGESTFU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGESTAR",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGESPED",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGESPDD",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGESHFU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGCOU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGBEA",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGBACHI",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_RENDIC",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_PROPEDEU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_OFICIO",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_NORMAL",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_MIETNICA",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_ING_ES15",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGPACE",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGESTFU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGESTAR",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGESPED",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGESPDD",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGESHFU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGCOU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGBEA",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGBACHI",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CUPOS",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CIENCIAS",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TIPO_INGRESO",
                table: "STG_REPOSTULACION_CARR_POND",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "REPOSTULANTE",
                table: "STG_REPOSTULACION_CARR_POND",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "EXAMEN",
                table: "STG_REPOSTULACION_CARR_POND",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ELIMINAR",
                table: "STG_REPOSTULACION_CARR_POND",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ANTECEDENTES",
                table: "STG_REPOSTULACION_CARR_POND",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO",
                table: "STG_PAIS_NACIONALIDAD",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "AGNO_PROCESO",
                table: "STG_PAIS_NACIONALIDAD",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "HABILITADO",
                table: "STG_REPOSTULACION_TIPOINGRESO",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ESPECIAL_REPOSTULACION",
                table: "STG_REPOSTULACION_TIPOINGRESO",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TIPO_INGRESO",
                table: "STG_REPOSTULACION_SELECCION",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "OPORTUNIDAD",
                table: "STG_REPOSTULACION_SELECCION",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ELIMINADO",
                table: "STG_REPOSTULACION_SELECCION",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ANIO_INGRESO",
                table: "STG_REPOSTULACION_SELECCION",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ID_REGION",
                table: "STG_REPOSTULACION_PROVINCIAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO",
                table: "STG_REPOSTULACION_PROVINCIAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ID_PROVINCIA",
                table: "STG_REPOSTULACION_COMUNAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO",
                table: "STG_REPOSTULACION_COMUNAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RANKING",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NEM",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MATEMATICAS",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LYC",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HISTORIA_Y_GEOGRAFIA",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EXCEDENTES",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_RENDIC",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_PROPEDEU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_OFICIO",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_NORMAL",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_MIETNICA",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_ING_ES15",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGPACE",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGESTFU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGESTAR",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGESPED",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGESPDD",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGESHFU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGCOU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGBEA",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_T_INGBACHI",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_RENDIC",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_PROPEDEU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_OFICIO",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_NORMAL",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_MIETNICA",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_ING_ES15",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGPACE",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGESTFU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGESTAR",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGESPED",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGESPDD",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGESHFU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGCOU",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGBEA",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "C_R_INGBACHI",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CUPOS",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CIENCIAS",
                table: "STG_REPOSTULACION_CARRERAS",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TIPO_INGRESO",
                table: "STG_REPOSTULACION_CARR_POND",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "REPOSTULANTE",
                table: "STG_REPOSTULACION_CARR_POND",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EXAMEN",
                table: "STG_REPOSTULACION_CARR_POND",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ELIMINAR",
                table: "STG_REPOSTULACION_CARR_POND",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ANTECEDENTES",
                table: "STG_REPOSTULACION_CARR_POND",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO",
                table: "STG_PAIS_NACIONALIDAD",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AGNO_PROCESO",
                table: "STG_PAIS_NACIONALIDAD",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
