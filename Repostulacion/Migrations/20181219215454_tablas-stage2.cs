﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Repostulacion.Migrations
{
    public partial class tablasstage2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "UNIDAD_EDUCATIVA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "SITUACION_EGRESO_EDUCACIONAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "REPOSTULANTE",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "RBD",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PUNTAJE_RANKING",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PUNTAJE_NEM",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_MATEMATICA_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_MATEMATICAS_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_LENGUAJE_COMUNIC_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_LENGUAJE_COMUNIC_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_HIST_CS_SOCIALES_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_HIST_CS_SOCIALES_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_CIENCIAS_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_CIENCIAS_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PROMEDIO_PAA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PROMEDIO_NOTAS",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PROMEDIO_LENG_MAT_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PROMEDIO_LENG_MAT_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PERCENTIL_LM_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PERCENTIL_LM_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PCE_QUIMICA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PCE_MATEMATICA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PCE_HISTORIA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PCE_FISICA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PCE_CS",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PCE_BIOLOGIA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PAA_VERBAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PAA_MATEMATICA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "LOCAL_EDUCACIONAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "GRUPO_DEPENDENCIA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "COD_ENSENANZA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "COD_COMUNA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO_REGION",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO_PROVINCIA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ANNIO_PROCESO",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ANNIO_EGRESO_E_MEDIA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ZONA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "VIVE_CON_HIJOS",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "VIVE_CON",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "VIVEN_PADRES",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "UNIDAD_EDUCATIVA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TRABAJAN_GRUPO_FAMILIAR",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TIPO_ORGANISMO_TRABAJO_PADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TIPO_ORGANISMO_TRABAJO_MADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TIPO_INSTITUCION",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TIPO_DISCAPACIDAD",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TIENE_TRABAJO_REMUNERADO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TIENE_HIJOS",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "SIT_OCUPACIONAL_PADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "SIT_OCUPACIONAL_MADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "RINDIO_PROCESO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "REPOSTULANTE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "RBD",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "RAMA_OCUPACIONAL_PADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "RAMA_OCUPACIONAL_MADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PERSONAS_ESTUDIAN_SUP",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "OCUPACION_PRINCIPAL_PADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "OCUPACION_PRINCIPAL_MADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "NRO_TARJETA_MATRICULA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_PORTUGUES",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_OTRO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_ITALIANO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_INGLES",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_FRANCES",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_ESPANOL",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_CHINO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_ALEMAN",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "LOCAL_EDUCACIONAL",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "JEFE_FAMILIA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "INSCRITO_PROCESO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "INGRESO_BRUTO_FAMILIAR",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "HORAS_DEDICA_TRABAJO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "HORARIO_TRABAJO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "GRUPO_FAMILIAR",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "GENERO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "FINANCIA_SECUNDARIO_ESTUDIOS",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "FINANCIA_PRIMARIO_ESTUDIOS",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ESTUD_GRUPO_FAMILIAR_SUPERIOR",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ESTUD_GRUPO_FAMILIAR_PREBASICA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ESTUD_GRUPO_FAMILIAR_OTROS",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ESTUD_GRUPO_FAMILIAR_MEDIA4",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ESTUD_GRUPO_FAMILIAR_MEDIA1_3",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ESTUD_GRUPO_FAMILIAR_BASICA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ESTADO_CIVIL",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "EMBARAZO_ADOLESCENTE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "EDUCACION_PADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "EDUCACION_MADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ECONOMICAMENTE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DOMINA_LENGUA_INDIGENA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DIR_NUMERO_TELEFONO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DIR_NUMERO_CELULAR",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DIR_CODIGO_REGION",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DIR_CODIGO_PROVINCIA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DIR_CODIGO_COMUNA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CUANTOS_HIJOS",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "COD_NACIONALIDAD",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "COD_ENSENANZA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "COD_COMUNA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO_REGION",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO_PROVINCIA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO_COMUNA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "COBERTURA_SALUD",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ANNIO_LLEGA_CHILE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ANNIO_EGRESO_E_MEDIA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "AGNO_PROCESO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ACTIVIDAD_JEFE_FAMILIA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "UNIDAD_EDUCATIVA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SITUACION_EGRESO_EDUCACIONAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "REPOSTULANTE",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RBD",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PUNTAJE_RANKING",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PUNTAJE_NEM",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_MATEMATICA_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_MATEMATICAS_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_LENGUAJE_COMUNIC_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_LENGUAJE_COMUNIC_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_HIST_CS_SOCIALES_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_HIST_CS_SOCIALES_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_CIENCIAS_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PTJE_CIENCIAS_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PROMEDIO_PAA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PROMEDIO_NOTAS",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PROMEDIO_LENG_MAT_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PROMEDIO_LENG_MAT_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PERCENTIL_LM_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PERCENTIL_LM_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PCE_QUIMICA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PCE_MATEMATICA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PCE_HISTORIA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PCE_FISICA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PCE_CS",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PCE_BIOLOGIA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PAA_VERBAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PAA_MATEMATICA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LOCAL_EDUCACIONAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "GRUPO_DEPENDENCIA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "COD_ENSENANZA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "COD_COMUNA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO_REGION",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO_PROVINCIA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ANNIO_PROCESO",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ANNIO_EGRESO_E_MEDIA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ZONA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "VIVE_CON_HIJOS",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "VIVE_CON",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "VIVEN_PADRES",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UNIDAD_EDUCATIVA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TRABAJAN_GRUPO_FAMILIAR",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TIPO_ORGANISMO_TRABAJO_PADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TIPO_ORGANISMO_TRABAJO_MADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TIPO_INSTITUCION",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TIPO_DISCAPACIDAD",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TIENE_TRABAJO_REMUNERADO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TIENE_HIJOS",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SIT_OCUPACIONAL_PADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SIT_OCUPACIONAL_MADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RINDIO_PROCESO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "REPOSTULANTE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RBD",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RAMA_OCUPACIONAL_PADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RAMA_OCUPACIONAL_MADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PERSONAS_ESTUDIAN_SUP",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "OCUPACION_PRINCIPAL_PADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "OCUPACION_PRINCIPAL_MADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NRO_TARJETA_MATRICULA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_PORTUGUES",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_OTRO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_ITALIANO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_INGLES",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_FRANCES",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_ESPANOL",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_CHINO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NIVEL_ALEMAN",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LOCAL_EDUCACIONAL",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "JEFE_FAMILIA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "INSCRITO_PROCESO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "INGRESO_BRUTO_FAMILIAR",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HORAS_DEDICA_TRABAJO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HORARIO_TRABAJO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "GRUPO_FAMILIAR",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "GENERO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FINANCIA_SECUNDARIO_ESTUDIOS",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FINANCIA_PRIMARIO_ESTUDIOS",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ESTUD_GRUPO_FAMILIAR_SUPERIOR",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ESTUD_GRUPO_FAMILIAR_PREBASICA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ESTUD_GRUPO_FAMILIAR_OTROS",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ESTUD_GRUPO_FAMILIAR_MEDIA4",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ESTUD_GRUPO_FAMILIAR_MEDIA1_3",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ESTUD_GRUPO_FAMILIAR_BASICA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ESTADO_CIVIL",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EMBARAZO_ADOLESCENTE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EDUCACION_PADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EDUCACION_MADRE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ECONOMICAMENTE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DOMINA_LENGUA_INDIGENA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DIR_NUMERO_TELEFONO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DIR_NUMERO_CELULAR",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DIR_CODIGO_REGION",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DIR_CODIGO_PROVINCIA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DIR_CODIGO_COMUNA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CUANTOS_HIJOS",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "COD_NACIONALIDAD",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "COD_ENSENANZA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "COD_COMUNA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO_REGION",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO_PROVINCIA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CODIGO_COMUNA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "COBERTURA_SALUD",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ANNIO_LLEGA_CHILE",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ANNIO_EGRESO_E_MEDIA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AGNO_PROCESO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ACTIVIDAD_JEFE_FAMILIA",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
