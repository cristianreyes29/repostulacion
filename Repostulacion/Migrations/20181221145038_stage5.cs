﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Repostulacion.Migrations
{
    public partial class stage5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "PROMEDIO_PAA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "PROMEDIO_NOTAS",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "PROMEDIO_LENG_MAT_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "PROMEDIO_LENG_MAT_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "PROMEDIO_PAA",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PROMEDIO_NOTAS",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PROMEDIO_LENG_MAT_ANTERIOR",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PROMEDIO_LENG_MAT_ACTUAL",
                table: "STG_DEMRE_ARCHIVO_C",
                nullable: true,
                oldClrType: typeof(float),
                oldNullable: true);
        }
    }
}
