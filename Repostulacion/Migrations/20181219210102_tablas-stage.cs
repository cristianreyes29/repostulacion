﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Repostulacion.Migrations
{
    public partial class tablasstage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "STG_DEMRE_ARCHIVO_B",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ACEPTA_CONTACTO_DEMRE = table.Column<string>(nullable: true),
                    ACTIVIDAD_JEFE_FAMILIA = table.Column<int>(nullable: false),
                    ACTIVIDAD_VOLUNTARIADO = table.Column<string>(nullable: true),
                    ACTIVIDA_NINGUNA = table.Column<string>(nullable: true),
                    ACT_AGRUPACION_DEPORTIVA = table.Column<string>(nullable: true),
                    ACT_AGRUPACION_POLITICA = table.Column<string>(nullable: true),
                    ACT_AGRUPACION_RELIGIOSA = table.Column<string>(nullable: true),
                    ACT_TRABAJO_NO_REMUNERADO = table.Column<string>(nullable: true),
                    AGNO_PROCESO = table.Column<int>(nullable: false),
                    ANNIO_EGRESO_E_MEDIA = table.Column<int>(nullable: false),
                    ANNIO_LLEGA_CHILE = table.Column<int>(nullable: false),
                    APELLIDO_MATERNO = table.Column<string>(nullable: true),
                    APELLIDO_PATERNO = table.Column<string>(nullable: true),
                    BEA = table.Column<string>(nullable: true),
                    CARRERA_INSTITUCION_SUPERIOR = table.Column<string>(nullable: true),
                    COBERTURA_SALUD = table.Column<int>(nullable: false),
                    CODIGO_COMUNA = table.Column<int>(nullable: false),
                    CODIGO_ETNIA = table.Column<string>(nullable: true),
                    CODIGO_INST_SUPERIOR_ANTERIOR = table.Column<string>(nullable: true),
                    CODIGO_PROVINCIA = table.Column<int>(nullable: false),
                    CODIGO_REGION = table.Column<int>(nullable: false),
                    COD_COMUNA = table.Column<int>(nullable: false),
                    COD_ENSENANZA = table.Column<int>(nullable: false),
                    COD_NACIONALIDAD = table.Column<int>(nullable: false),
                    COINCIDE_GENERO = table.Column<string>(nullable: true),
                    COMPLETO_EDUCACION_MADRE = table.Column<string>(nullable: true),
                    COMPLETO_EDUCACION_PADRE = table.Column<string>(nullable: true),
                    CUANTOS_HIJOS = table.Column<int>(nullable: false),
                    DESC_OTRO_IDIOMA = table.Column<string>(nullable: true),
                    DESC_OTRO_TIPO_CRED = table.Column<string>(nullable: true),
                    DIGITO_VERIFICADOR = table.Column<string>(nullable: true),
                    DIR_BLOCK = table.Column<string>(nullable: true),
                    DIR_CALLE = table.Column<string>(nullable: true),
                    DIR_CIUDAD = table.Column<string>(nullable: true),
                    DIR_CODIGO_AREA = table.Column<string>(nullable: true),
                    DIR_CODIGO_COMUNA = table.Column<int>(nullable: false),
                    DIR_CODIGO_PROVINCIA = table.Column<int>(nullable: false),
                    DIR_CODIGO_REGION = table.Column<int>(nullable: false),
                    DIR_DEPATAMENTO = table.Column<string>(nullable: true),
                    DIR_EMAIL = table.Column<string>(nullable: true),
                    DIR_NOMBRE_COMUNA = table.Column<string>(nullable: true),
                    DIR_NOMBRE_PROVINCIA = table.Column<string>(nullable: true),
                    DIR_NUMERO = table.Column<string>(nullable: true),
                    DIR_NUMERO_CELULAR = table.Column<int>(nullable: false),
                    DIR_NUMERO_TELEFONO = table.Column<int>(nullable: false),
                    DIR_PREFIJO_CELULAR = table.Column<string>(nullable: true),
                    DIR_VILLA_POBLACION = table.Column<string>(nullable: true),
                    DISCAPACIDAD = table.Column<string>(nullable: true),
                    DISCAPACIDAD_AUDITIVA = table.Column<string>(nullable: true),
                    DISCAPACIDAD_FISICA = table.Column<string>(nullable: true),
                    DISCAPACIDAD_INTELECTUAL = table.Column<string>(nullable: true),
                    DISCAPACIDAD_PSIQUICA = table.Column<string>(nullable: true),
                    DISCAPACIDAD_VISUAL = table.Column<string>(nullable: true),
                    DOMINA_LENGUA_INDIGENA = table.Column<int>(nullable: false),
                    ECONOMICAMENTE = table.Column<int>(nullable: false),
                    EDUCACION_MADRE = table.Column<int>(nullable: false),
                    EDUCACION_PADRE = table.Column<int>(nullable: false),
                    EMBARAZO_ADOLESCENTE = table.Column<int>(nullable: false),
                    ESTADO_CIVIL = table.Column<int>(nullable: false),
                    ESTUDIO_INST_SUPERIOR = table.Column<string>(nullable: true),
                    ESTUD_GRUPO_FAMILIAR_BASICA = table.Column<int>(nullable: false),
                    ESTUD_GRUPO_FAMILIAR_MEDIA1_3 = table.Column<int>(nullable: false),
                    ESTUD_GRUPO_FAMILIAR_MEDIA4 = table.Column<int>(nullable: false),
                    ESTUD_GRUPO_FAMILIAR_OTROS = table.Column<int>(nullable: false),
                    ESTUD_GRUPO_FAMILIAR_PREBASICA = table.Column<int>(nullable: false),
                    ESTUD_GRUPO_FAMILIAR_SUPERIOR = table.Column<int>(nullable: false),
                    FECHA_NACIMIENTO = table.Column<string>(nullable: true),
                    FINANCIA_PRIMARIO_ESTUDIOS = table.Column<int>(nullable: false),
                    FINANCIA_SECUNDARIO_ESTUDIOS = table.Column<int>(nullable: false),
                    GENERO = table.Column<int>(nullable: false),
                    GRUPO_FAMILIAR = table.Column<int>(nullable: false),
                    HORARIO_TRABAJO = table.Column<int>(nullable: false),
                    HORAS_DEDICA_TRABAJO = table.Column<int>(nullable: false),
                    IDIOMA_ALEMAN = table.Column<string>(nullable: true),
                    IDIOMA_CHINO = table.Column<string>(nullable: true),
                    IDIOMA_ESPANOL = table.Column<string>(nullable: true),
                    IDIOMA_FRANCES = table.Column<string>(nullable: true),
                    IDIOMA_INGLES = table.Column<string>(nullable: true),
                    IDIOMA_ITALIANO = table.Column<string>(nullable: true),
                    IDIOMA_OTRO = table.Column<string>(nullable: true),
                    IDIOMA_PORTUGUES = table.Column<string>(nullable: true),
                    INGRESO_BRUTO_FAMILIAR = table.Column<int>(nullable: false),
                    INSCRITO_PROCESO = table.Column<int>(nullable: false),
                    JEFE_FAMILIA = table.Column<int>(nullable: false),
                    LOCAL_EDUCACIONAL = table.Column<int>(nullable: false),
                    NACIONALIDAD = table.Column<string>(nullable: true),
                    NIVEL_ALEMAN = table.Column<int>(nullable: false),
                    NIVEL_CHINO = table.Column<int>(nullable: false),
                    NIVEL_ESPANOL = table.Column<int>(nullable: false),
                    NIVEL_FRANCES = table.Column<int>(nullable: false),
                    NIVEL_INGLES = table.Column<int>(nullable: false),
                    NIVEL_ITALIANO = table.Column<int>(nullable: false),
                    NIVEL_OTRO = table.Column<int>(nullable: false),
                    NIVEL_PORTUGUES = table.Column<int>(nullable: false),
                    NOMBRES = table.Column<string>(nullable: true),
                    NOMBRE_COMUNA = table.Column<string>(nullable: true),
                    NOMBRE_PROVINCIA = table.Column<string>(nullable: true),
                    NOMBRE_SOCIAL = table.Column<string>(nullable: true),
                    NRO_TARJETA_MATRICULA = table.Column<int>(nullable: false),
                    OCUPACION = table.Column<string>(nullable: true),
                    OCUPACION_PRINCIPAL_MADRE = table.Column<int>(nullable: false),
                    OCUPACION_PRINCIPAL_PADRE = table.Column<int>(nullable: false),
                    OTRO_JEFE_FAMILIA = table.Column<string>(nullable: true),
                    PACE = table.Column<string>(nullable: true),
                    PERSONAS_ESTUDIAN_SUP = table.Column<int>(nullable: false),
                    POSTULAR_BECA = table.Column<string>(nullable: true),
                    POSTULAR_CAE = table.Column<string>(nullable: true),
                    POSTULAR_CREDITO_SOLIDARIO = table.Column<string>(nullable: true),
                    POSTULAR_GRATUIDAD = table.Column<string>(nullable: true),
                    POSTULAR_NINGUN_FINAN = table.Column<string>(nullable: true),
                    POSTULAR_OTRO_TIPO_CRED = table.Column<string>(nullable: true),
                    PREPARACION_COLEGIO = table.Column<string>(nullable: true),
                    PREPARACION_ESTUDIO_PERSONAL = table.Column<string>(nullable: true),
                    PREPARACION_NINGUNA = table.Column<string>(nullable: true),
                    PREPARACION_PREU_GRATUITO = table.Column<string>(nullable: true),
                    PREPARACION_PREU_PAGADO = table.Column<string>(nullable: true),
                    PREPARACION_PROF_PARTICULAR = table.Column<string>(nullable: true),
                    PREPARACION_SITIO_DEMRE = table.Column<string>(nullable: true),
                    PUBLICA_NOMBRE_SOCIAL = table.Column<string>(nullable: true),
                    PUBLICA_PTJE_COL = table.Column<string>(nullable: true),
                    PUBLICA_PTJE_NAC = table.Column<string>(nullable: true),
                    RAMA_OCUPACIONAL_MADRE = table.Column<int>(nullable: false),
                    RAMA_OCUPACIONAL_PADRE = table.Column<int>(nullable: false),
                    RBD = table.Column<int>(nullable: false),
                    RECIBIO_ADECUACION_COLEGIO = table.Column<string>(nullable: true),
                    REPOSTULANTE = table.Column<int>(nullable: false),
                    RINDIO_PROCESO = table.Column<int>(nullable: false),
                    RUT = table.Column<string>(nullable: true),
                    RUT_MADRE = table.Column<string>(nullable: true),
                    RUT_PADRE = table.Column<string>(nullable: true),
                    SECRETARIA_ADMISION = table.Column<string>(nullable: true),
                    SIT_OCUPACIONAL_MADRE = table.Column<int>(nullable: false),
                    SIT_OCUPACIONAL_PADRE = table.Column<int>(nullable: false),
                    SOLICITA_ADECUACION = table.Column<string>(nullable: true),
                    TEXTO_ADECUACION = table.Column<string>(nullable: true),
                    TIENE_HIJOS = table.Column<int>(nullable: false),
                    TIENE_TRABAJO_REMUNERADO = table.Column<int>(nullable: false),
                    TIPO_DISCAPACIDAD = table.Column<int>(nullable: false),
                    TIPO_IDENTIFICACION = table.Column<string>(nullable: true),
                    TIPO_INSTITUCION = table.Column<int>(nullable: false),
                    TIPO_ORGANISMO_TRABAJO_MADRE = table.Column<int>(nullable: false),
                    TIPO_ORGANISMO_TRABAJO_PADRE = table.Column<int>(nullable: false),
                    TITULACION_CARRERA = table.Column<string>(nullable: true),
                    TRABAJAN_GRUPO_FAMILIAR = table.Column<int>(nullable: false),
                    UNIDAD_EDUCATIVA = table.Column<int>(nullable: false),
                    VIVEN_PADRES = table.Column<int>(nullable: false),
                    VIVE_CON = table.Column<int>(nullable: false),
                    VIVE_CON_HIJOS = table.Column<int>(nullable: false),
                    ZIP_CODE = table.Column<string>(nullable: true),
                    ZONA = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STG_DEMRE_ARCHIVO_B", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "STG_DEMRE_ARCHIVO_C",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ANNIO_EGRESO_E_MEDIA = table.Column<int>(nullable: false),
                    ANNIO_PROCESO = table.Column<int>(nullable: false),
                    BEA = table.Column<string>(nullable: true),
                    CODIGO_PROVINCIA = table.Column<int>(nullable: false),
                    CODIGO_REGION = table.Column<int>(nullable: false),
                    COD_COMUNA = table.Column<int>(nullable: false),
                    COD_ENSENANZA = table.Column<int>(nullable: false),
                    DV = table.Column<string>(nullable: true),
                    GRUPO_DEPENDENCIA = table.Column<int>(nullable: false),
                    HABILITA_PEDAGOGIA = table.Column<string>(nullable: true),
                    LOCAL_EDUCACIONAL = table.Column<int>(nullable: false),
                    MODULO_CIENCIAS_ACTUAL = table.Column<string>(nullable: true),
                    MODULO_CIENCIAS_ANTERIOR = table.Column<string>(nullable: true),
                    PAA_MATEMATICA = table.Column<int>(nullable: false),
                    PAA_VERBAL = table.Column<int>(nullable: false),
                    PACE = table.Column<string>(nullable: true),
                    PCE_BIOLOGIA = table.Column<int>(nullable: false),
                    PCE_CS = table.Column<int>(nullable: false),
                    PCE_FISICA = table.Column<int>(nullable: false),
                    PCE_HISTORIA = table.Column<int>(nullable: false),
                    PCE_MATEMATICA = table.Column<int>(nullable: false),
                    PCE_QUIMICA = table.Column<int>(nullable: false),
                    PERCENTIL_LM_ACTUAL = table.Column<int>(nullable: false),
                    PERCENTIL_LM_ANTERIOR = table.Column<int>(nullable: false),
                    PERCENTIL_NOTAS = table.Column<string>(nullable: true),
                    PROMEDIO_LENG_MAT_ACTUAL = table.Column<int>(nullable: false),
                    PROMEDIO_LENG_MAT_ANTERIOR = table.Column<int>(nullable: false),
                    PROMEDIO_NOTAS = table.Column<int>(nullable: false),
                    PROMEDIO_PAA = table.Column<int>(nullable: false),
                    PTJE_CIENCIAS_ACTUAL = table.Column<int>(nullable: false),
                    PTJE_CIENCIAS_ANTERIOR = table.Column<int>(nullable: false),
                    PTJE_HIST_CS_SOCIALES_ACTUAL = table.Column<int>(nullable: false),
                    PTJE_HIST_CS_SOCIALES_ANTERIOR = table.Column<int>(nullable: false),
                    PTJE_LENGUAJE_COMUNIC_ACTUAL = table.Column<int>(nullable: false),
                    PTJE_LENGUAJE_COMUNIC_ANTERIOR = table.Column<int>(nullable: false),
                    PTJE_MATEMATICAS_ANTERIOR = table.Column<int>(nullable: false),
                    PTJE_MATEMATICA_ACTUAL = table.Column<int>(nullable: false),
                    PUNTAJE_NEM = table.Column<int>(nullable: false),
                    PUNTAJE_RANKING = table.Column<int>(nullable: false),
                    RAMA_EDUCACIONAL = table.Column<string>(nullable: true),
                    RBD = table.Column<int>(nullable: false),
                    REPOSTULANTE = table.Column<int>(nullable: false),
                    RUT = table.Column<string>(nullable: true),
                    SITUACION_EGRESO_EDUCACIONAL = table.Column<int>(nullable: false),
                    TIPO_IDENTIFICACION = table.Column<string>(nullable: true),
                    UNIDAD_EDUCATIVA = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STG_DEMRE_ARCHIVO_C", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "STG_PAIS_NACIONALIDAD",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AGNO_PROCESO = table.Column<int>(nullable: false),
                    CODIGO = table.Column<int>(nullable: false),
                    DESCRIPCION = table.Column<string>(nullable: true),
                    DOMINIO = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STG_PAIS_NACIONALIDAD", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "STG_REPOSTULACION_CARR_POND",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ANIO_INGRESO = table.Column<int>(nullable: false),
                    ANTECEDENTES = table.Column<int>(nullable: false),
                    CARRERA1 = table.Column<string>(nullable: true),
                    CARRERA2 = table.Column<string>(nullable: true),
                    ELIMINAR = table.Column<int>(nullable: false),
                    EXAMEN = table.Column<int>(nullable: false),
                    FECHA_INGRESO = table.Column<string>(nullable: true),
                    FECHA_MODIFICACION = table.Column<string>(nullable: true),
                    INSCRITO_OTRA_UNIVERSIDAD = table.Column<string>(nullable: true),
                    PONDERADO1 = table.Column<float>(nullable: false),
                    PONDERADO2 = table.Column<float>(nullable: false),
                    POND_ADICIONAL1 = table.Column<float>(nullable: false),
                    POND_ADICIONAL2 = table.Column<float>(nullable: false),
                    RAMA = table.Column<string>(nullable: true),
                    REPOSTULANTE = table.Column<int>(nullable: false),
                    RUT = table.Column<string>(nullable: false),
                    TIPO_INGRESO = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STG_REPOSTULACION_CARR_POND", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "STG_REPOSTULACION_CARRERAS",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CIENCIAS = table.Column<int>(nullable: false),
                    CODIGO_CARRERA = table.Column<string>(nullable: true),
                    CUPOS = table.Column<int>(nullable: false),
                    C_R_INGBACHI = table.Column<int>(nullable: false),
                    C_R_INGBEA = table.Column<int>(nullable: false),
                    C_R_INGCOU = table.Column<int>(nullable: false),
                    C_R_INGESHFU = table.Column<int>(nullable: false),
                    C_R_INGESPDD = table.Column<int>(nullable: false),
                    C_R_INGESPED = table.Column<int>(nullable: false),
                    C_R_INGESTAR = table.Column<int>(nullable: false),
                    C_R_INGESTFU = table.Column<int>(nullable: false),
                    C_R_INGPACE = table.Column<int>(nullable: false),
                    C_R_ING_ES15 = table.Column<int>(nullable: false),
                    C_R_MIETNICA = table.Column<int>(nullable: false),
                    C_R_NORMAL = table.Column<int>(nullable: false),
                    C_R_OFICIO = table.Column<int>(nullable: false),
                    C_R_PROPEDEU = table.Column<int>(nullable: false),
                    C_R_RENDIC = table.Column<int>(nullable: false),
                    C_T_INGBACHI = table.Column<int>(nullable: false),
                    C_T_INGBEA = table.Column<int>(nullable: false),
                    C_T_INGCOU = table.Column<int>(nullable: false),
                    C_T_INGESHFU = table.Column<int>(nullable: false),
                    C_T_INGESPDD = table.Column<int>(nullable: false),
                    C_T_INGESPED = table.Column<int>(nullable: false),
                    C_T_INGESTAR = table.Column<int>(nullable: false),
                    C_T_INGESTFU = table.Column<int>(nullable: false),
                    C_T_INGPACE = table.Column<int>(nullable: false),
                    C_T_ING_ES15 = table.Column<int>(nullable: false),
                    C_T_MIETNICA = table.Column<int>(nullable: false),
                    C_T_NORMAL = table.Column<int>(nullable: false),
                    C_T_OFICIO = table.Column<int>(nullable: false),
                    C_T_PROPEDEU = table.Column<int>(nullable: false),
                    C_T_RENDIC = table.Column<int>(nullable: false),
                    EXCEDENTES = table.Column<int>(nullable: false),
                    HISTORIA_Y_GEOGRAFIA = table.Column<int>(nullable: false),
                    ID_CARRERA = table.Column<int>(nullable: false),
                    LYC = table.Column<int>(nullable: false),
                    MATEMATICAS = table.Column<int>(nullable: false),
                    NEM = table.Column<int>(nullable: false),
                    NOMBRE_CARRERA = table.Column<string>(nullable: true),
                    RANKING = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STG_REPOSTULACION_CARRERAS", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "STG_REPOSTULACION_COMUNAS",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CODIGO = table.Column<int>(nullable: false),
                    ID_COMUNA = table.Column<int>(nullable: false),
                    ID_PROVINCIA = table.Column<int>(nullable: false),
                    NOMBRE = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STG_REPOSTULACION_COMUNAS", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "STG_REPOSTULACION_ESTADOCIVIL",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CODIGO_ESTADO = table.Column<int>(nullable: false),
                    DETALLE = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STG_REPOSTULACION_ESTADOCIVIL", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "STG_REPOSTULACION_PROVINCIAS",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CODIGO = table.Column<int>(nullable: false),
                    ID_PROVINCIA = table.Column<int>(nullable: false),
                    ID_REGION = table.Column<int>(nullable: false),
                    NOMBRE = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STG_REPOSTULACION_PROVINCIAS", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "STG_REPOSTULACION_REGIONES",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CODIGO = table.Column<string>(nullable: true),
                    ID_REGION = table.Column<int>(nullable: false),
                    NOMBRE = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STG_REPOSTULACION_REGIONES", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "STG_REPOSTULACION_REPOSTULANTE",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DETALLE = table.Column<string>(nullable: true),
                    ID_TIPO = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STG_REPOSTULACION_REPOSTULANTE", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "STG_REPOSTULACION_SELECCION",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ANIO_INGRESO = table.Column<int>(nullable: false),
                    CARRERA = table.Column<string>(nullable: true),
                    ELIMINADO = table.Column<int>(nullable: false),
                    OPORTUNIDAD = table.Column<int>(nullable: false),
                    PONDERADO = table.Column<float>(nullable: false),
                    RUT = table.Column<string>(nullable: true),
                    TIPO_INGRESO = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STG_REPOSTULACION_SELECCION", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "STG_REPOSTULACION_TIPOINGRESO",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CODIGO = table.Column<string>(nullable: true),
                    ESPECIAL_REPOSTULACION = table.Column<int>(nullable: false),
                    HABILITADO = table.Column<int>(nullable: false),
                    ID_TIPO = table.Column<int>(nullable: false),
                    NOMBRE = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STG_REPOSTULACION_TIPOINGRESO", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "STG_DEMRE_ARCHIVO_B");

            migrationBuilder.DropTable(
                name: "STG_DEMRE_ARCHIVO_C");

            migrationBuilder.DropTable(
                name: "STG_PAIS_NACIONALIDAD");

            migrationBuilder.DropTable(
                name: "STG_REPOSTULACION_CARR_POND");

            migrationBuilder.DropTable(
                name: "STG_REPOSTULACION_CARRERAS");

            migrationBuilder.DropTable(
                name: "STG_REPOSTULACION_COMUNAS");

            migrationBuilder.DropTable(
                name: "STG_REPOSTULACION_ESTADOCIVIL");

            migrationBuilder.DropTable(
                name: "STG_REPOSTULACION_PROVINCIAS");

            migrationBuilder.DropTable(
                name: "STG_REPOSTULACION_REGIONES");

            migrationBuilder.DropTable(
                name: "STG_REPOSTULACION_REPOSTULANTE");

            migrationBuilder.DropTable(
                name: "STG_REPOSTULACION_SELECCION");

            migrationBuilder.DropTable(
                name: "STG_REPOSTULACION_TIPOINGRESO");
        }
    }
}
