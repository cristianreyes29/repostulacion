﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Repostulacion.Migrations
{
    public partial class tablasstage3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "DIR_NUMERO_TELEFONO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DIR_NUMERO_CELULAR",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "DIR_NUMERO_TELEFONO",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DIR_NUMERO_CELULAR",
                table: "STG_DEMRE_ARCHIVO_B",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
