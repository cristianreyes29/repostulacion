﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Repostulacion.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Controlador",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Codigo = table.Column<int>(nullable: false),
                    Nombre = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Controlador", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Menu",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Menu", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Metodo",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Codigo = table.Column<int>(nullable: false),
                    Nombre = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Metodo", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Rol",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rol", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NombreUsuario = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "relControladorMetodo",
                columns: table => new
                {
                    ControladorID = table.Column<int>(nullable: false),
                    MetodoID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_relControladorMetodo", x => new { x.ControladorID, x.MetodoID });
                    table.ForeignKey(
                        name: "FK_relControladorMetodo_Controlador_ControladorID",
                        column: x => x.ControladorID,
                        principalTable: "Controlador",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_relControladorMetodo_Metodo_MetodoID",
                        column: x => x.MetodoID,
                        principalTable: "Metodo",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "relRolControladorMetodo",
                columns: table => new
                {
                    RolID = table.Column<int>(nullable: false),
                    ControladorID = table.Column<int>(nullable: false),
                    MetodoID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_relRolControladorMetodo", x => new { x.RolID, x.ControladorID, x.MetodoID });
                    table.ForeignKey(
                        name: "FK_relRolControladorMetodo_Controlador_ControladorID",
                        column: x => x.ControladorID,
                        principalTable: "Controlador",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_relRolControladorMetodo_Metodo_MetodoID",
                        column: x => x.MetodoID,
                        principalTable: "Metodo",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_relRolControladorMetodo_Rol_RolID",
                        column: x => x.RolID,
                        principalTable: "Rol",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "relRolMenu",
                columns: table => new
                {
                    RolID = table.Column<int>(nullable: false),
                    MenuID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_relRolMenu", x => new { x.RolID, x.MenuID });
                    table.ForeignKey(
                        name: "FK_relRolMenu_Menu_MenuID",
                        column: x => x.MenuID,
                        principalTable: "Menu",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_relRolMenu_Rol_RolID",
                        column: x => x.RolID,
                        principalTable: "Rol",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "relUsuarioRol",
                columns: table => new
                {
                    UsuarioID = table.Column<int>(nullable: false),
                    RolID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_relUsuarioRol", x => new { x.UsuarioID, x.RolID });
                    table.ForeignKey(
                        name: "FK_relUsuarioRol_Rol_RolID",
                        column: x => x.RolID,
                        principalTable: "Rol",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_relUsuarioRol_Usuario_UsuarioID",
                        column: x => x.UsuarioID,
                        principalTable: "Usuario",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Controlador_Codigo",
                table: "Controlador",
                column: "Codigo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Metodo_Codigo",
                table: "Metodo",
                column: "Codigo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_relControladorMetodo_MetodoID",
                table: "relControladorMetodo",
                column: "MetodoID");

            migrationBuilder.CreateIndex(
                name: "IX_relRolControladorMetodo_ControladorID",
                table: "relRolControladorMetodo",
                column: "ControladorID");

            migrationBuilder.CreateIndex(
                name: "IX_relRolControladorMetodo_MetodoID",
                table: "relRolControladorMetodo",
                column: "MetodoID");

            migrationBuilder.CreateIndex(
                name: "IX_relRolMenu_MenuID",
                table: "relRolMenu",
                column: "MenuID");

            migrationBuilder.CreateIndex(
                name: "IX_relUsuarioRol_RolID",
                table: "relUsuarioRol",
                column: "RolID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "relControladorMetodo");

            migrationBuilder.DropTable(
                name: "relRolControladorMetodo");

            migrationBuilder.DropTable(
                name: "relRolMenu");

            migrationBuilder.DropTable(
                name: "relUsuarioRol");

            migrationBuilder.DropTable(
                name: "Controlador");

            migrationBuilder.DropTable(
                name: "Metodo");

            migrationBuilder.DropTable(
                name: "Menu");

            migrationBuilder.DropTable(
                name: "Rol");

            migrationBuilder.DropTable(
                name: "Usuario");
        }
    }
}
