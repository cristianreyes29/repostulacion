﻿using Microsoft.EntityFrameworkCore;
using Repostulacion.Models;

namespace Repostulacion.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Rol> Roles { get; set; }
        public DbSet<relUsuarioRol> relUsuarioRol { get; set; }

        public DbSet<Controlador> Controladores { get; set; }
        public DbSet<Metodo> Metodos { get; set; }
        public DbSet<relControladorMetodo> relControladorMetodo { get; set; }

        public DbSet<relRolControladorMetodo> relRolControladorMetodo { get; set; }

        public DbSet<Menu> Menus { get; set; }

        public DbSet<relRolMenu> relRolMenu { get; set; }


        public DbSet<STG_REPOSTULACION_SELECCION> STG_REPOSTULACION_SELECCION { get; set; }
        public DbSet<STG_REPOSTULACION_CARR_POND> STG_REPOSTULACION_CARR_POND { get; set; }
        public DbSet<STG_REPOSTULACION_CARRERAS> STG_REPOSTULACION_CARRERAS { get; set; }
        public DbSet<STG_REPOSTULACION_TIPOINGRESO> STG_REPOSTULACION_TIPOINGRESO { get; set; }
        public DbSet<STG_REPOSTULACION_REPOSTULANTE> STG_REPOSTULACION_REPOSTULANTE { get; set; }
        public DbSet<STG_REPOSTULACION_ESTADOCIVIL> STG_REPOSTULACION_ESTADOCIVIL { get; set; }
        public DbSet<STG_PAIS_NACIONALIDAD> STG_PAIS_NACIONALIDAD { get; set; }
        public DbSet<STG_REPOSTULACION_REGIONES> STG_REPOSTULACION_REGIONES { get; set; }
        public DbSet<STG_REPOSTULACION_PROVINCIAS> STG_REPOSTULACION_PROVINCIAS { get; set; }
        public DbSet<STG_REPOSTULACION_COMUNAS> STG_REPOSTULACION_COMUNAS { get; set; }
        public DbSet<STG_DEMRE_ARCHIVO_B> STG_DEMRE_ARCHIVO_B { get; set; }
        public DbSet<STG_DEMRE_ARCHIVO_C> STG_DEMRE_ARCHIVO_C { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<relUsuarioRol>().HasKey(r => new { r.UsuarioID, r.RolID });
            builder.Entity<relControladorMetodo>().HasKey(r => new { r.ControladorID, r.MetodoID });
            builder.Entity<relRolControladorMetodo>().HasKey(r => new { r.RolID, r.ControladorID, r.MetodoID });
            builder.Entity<relRolMenu>().HasKey(r => new { r.RolID, r.MenuID });

            builder.Entity<Controlador>().HasIndex(c => c.Codigo).IsUnique();
            builder.Entity<Metodo>().HasIndex(m => m.Codigo).IsUnique();
        }
    }
}
