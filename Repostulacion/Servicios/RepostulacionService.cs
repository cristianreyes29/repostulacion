﻿using Repostulacion.Data;
using Repostulacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repostulacion.Servicios
{
    public class RepostulacionService
    {
        public readonly ApplicationDbContext db;

        public RepostulacionService(ApplicationDbContext context)
        {
            db = context;
        }

        public void ObtenerCarrerasPorTipoIngreso()
        {

            List<STG_REPOSTULACION_CARRERAS> listaCarerras = (from carrera in db.STG_REPOSTULACION_CARRERAS
                                                              select carrera).ToList();

            var lista1 = (from cpon in db.STG_REPOSTULACION_CARR_POND
                          where cpon.INSCRITO_OTRA_UNIVERSIDAD.Equals("0") && cpon.ELIMINAR == 0 &&
                          cpon.POND_ADICIONAL1 == 0
                          select new
                          {
                              cpon.RUT,
                              cpon.TIPO_INGRESO,
                              CARRERA = cpon.CARRERA1,
                              PONDERADO = cpon.PONDERADO1

                          }).ToList();

            var lista2 = (from cpon in db.STG_REPOSTULACION_CARR_POND
                          where (cpon.CARRERA2 != null || !cpon.CARRERA2.Equals("0")) &&
                          cpon.INSCRITO_OTRA_UNIVERSIDAD.Equals("0") && cpon.ELIMINAR == 0 &&
                          cpon.POND_ADICIONAL1 == 0
                          select new
                          {
                              cpon.RUT,
                              cpon.TIPO_INGRESO,
                              CARRERA = cpon.CARRERA2,
                              PONDERADO = cpon.PONDERADO2

                          }).ToList();

            var lista3 = (lista1).Union(lista2);

            var listaPostulaciones = (from p in lista3
                                      select p).OrderBy(p => p.CARRERA).OrderByDescending(p => p.PONDERADO).ToList();


            int[] tiposIngreso = new int[16] { 1, 2, 3, 4, 5, 8, 9, 12, 13, 14, 15, 19, 23, 24, 26, 32 };

            int contadorCarreras = 0;
            foreach(var tipoIngreso in tiposIngreso)
            {
                contadorCarreras = 0;

                foreach(var carrera in listaCarerras)
                {
                    contadorCarreras++;

                }
            }



        }
    }
}
